package com.bbva.next.files;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ResourceFileReader implements Reader{
		
	public List<String> readLines(String fileName) throws IOException{
		List<String> content = null;
		
		if(fileName.endsWith(".txt")) {
			//Lee un archivo de verdad desde los resources
			try {
				Path filePath;
				filePath = Paths.get(this.getClass().getResource(fileName).toURI());
				content = Files.readAllLines(filePath, Charset.defaultCharset()); 
			} catch (URISyntaxException e) {}
			
		}
		
		return content;
	}

}
