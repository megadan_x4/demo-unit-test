package com.bbva.next.files;

import java.io.IOException;
import java.util.List;

public class LinesCounter {
	
	private Reader reader;
	
	/**
	 * Cuenta las lineas de un archivo
	 * @param fileName nombre del archivo
	 * @return numero de lineas en el archivo. 
	 * 			-1 si el archivo no es valido. -2 si el archivo no puede leerse
	 */
	public int countLines(String fileName) {
		List<String> lines;
		
		try {
			lines = reader.readLines(fileName);
			if(lines != null)
				return lines.size();
			
		} catch(IOException e) {
			return -2;
		}
		return -1;
	}

	public void setFileReader(Reader reader) {
		this.reader = reader;
	}
	
}
