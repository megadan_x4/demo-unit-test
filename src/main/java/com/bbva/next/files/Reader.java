package com.bbva.next.files;

import java.io.IOException;
import java.util.List;

public interface Reader {
	
	public List<String> readLines(String fileName) throws IOException;

}
