package com.bbva.next.calculator;

public class ComplexCalculator implements Calculator {
	
	public int sum(int a, int b) {
		for(int i = 0 ; i < b ; i++)
			a++;
		return a;
	}

	public int substract(int a, int b) {
		for(int i = 0 ; i < b ; i++)
			a--;
		return a;
	}

	public int multiply(int a, int b) {
		int res = a;
		for(int i = 1; i < b ; i++)
			res += a;
		return res;
	}

	public int divide(int a, int b) { //10 /3
		int res = 0;
		if(b==0)
			return 0;
		
		while(a >= b) {
			res++;
			a -= b;
		}
		return res;
	}

}
