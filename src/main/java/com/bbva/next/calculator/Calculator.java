package com.bbva.next.calculator;

public interface Calculator {
	
	public int sum(int a, int b);
	public int substract(int a, int b);
	public int multiply(int a, int b);
	public int divide(int a, int b);
}
	
