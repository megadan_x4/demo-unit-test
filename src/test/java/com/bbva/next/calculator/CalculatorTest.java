package com.bbva.next.calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class CalculatorTest {
	
	@Test
	void sumTest() {
		// Dado que...
		Calculator c = getCalculator();
		
		// Cuando
		int result = c.sum(2, 2);
		
		// Entonces
		assertEquals(4, result, "Dos mas dos igual a cuatro");
	}
	
	@Test
	void substractTest() {
		Calculator c = getCalculator();
		int result = c.substract(2, 2);
		assertEquals(0, result, "Dos menos dos igual a cero");
	}
	
	@Test
	void multiplyTest() {
		Calculator c = getCalculator();
		int result = c.multiply(2, 2);
		assertEquals(4, result, "Dos por dos igual a cuatro");
	}
	
	@Test
	void multiplyLargeNumberTest() {
		Calculator c = getCalculator();
		int result = c.multiply(10_000, 10_000);
		assertEquals(100_000_000, result, "Diez mil por diez mil es igual a cien millones");
	}
	
	@Test
	void multiplyByZero() {
		Calculator c = getCalculator();
		int result = c.multiply(Integer.MAX_VALUE, 0);
		assertEquals(0, result, "Cualquier numero por cero es igual a cero");
	}
	
	@Test
	void divideTest() {
		Calculator c = getCalculator();
		int result = c.divide(10, 5);
		assertEquals(2, result, "Diez entre 5 es igual a 2");
	}
	
	@Test
	void divideByZeroTest() {
		Calculator c = getCalculator();
		
		assertThrows(ArithmeticException.class,
				()-> c.divide(5, 0), "No se puede dividir entre cero");
	}
	
	private Calculator getCalculator() {
		return new ComplexCalculator();
	}
	

}
