package com.bbva.next.files.doubles;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.bbva.next.files.Reader;

public class FileReaderStub implements Reader {

	@Override
	public List<String> readLines(String fileName) throws IOException {
		return Arrays.asList("uno", "dos", "tres"); //Devolvemos siempre lo mismo
	}

}
