package com.bbva.next.files.doubles;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.bbva.next.files.Reader;

public class FakeFileReader implements Reader {

	public List<String> readLines(String fileName) throws IOException{
		List<String> content = null;
		
		if(fileName.endsWith(".txt")) {
			content = getLinesDummy(); //Regresamos un dummy
		}
		
		return content;
	}
	
	private List<String> getLinesDummy(){
		List<String> dummy = Arrays.asList("uno", "dos", "tres");
		return dummy;
	}

}
