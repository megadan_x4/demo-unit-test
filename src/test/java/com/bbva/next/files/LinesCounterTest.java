package com.bbva.next.files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.bbva.next.files.doubles.FakeFileReader;

public class LinesCounterTest {
	
	@Test
	void countLinesOnRealFileTest() throws URISyntaxException {
		
		LinesCounter counter = new LinesCounter();
		
		Reader realReader = new ResourceFileReader(); //Lee un archivo de verdad
		counter.setFileReader(realReader);
		
		int lines = counter.countLines("planetas.txt");
		
		assertEquals(9, lines, "Hay 9 planetas");
		
	}
	
	@Test
	void countLinesOnFakeFileTxtTest() {
		
		LinesCounter counter = new LinesCounter();
		
		Reader realReader = new FakeFileReader(); //Lee un archivo de verdad
		counter.setFileReader(realReader);
		
		int lines = counter.countLines("planetas.txt");
		
		assertTrue(lines > 0, "El fake regresa al menos una linea cuando lee un .txt");
		
	}
	
	@Test
	void countLinesOnFakeFileDatTest() {
		
		LinesCounter counter = new LinesCounter();
		
		Reader realReader = new FakeFileReader(); //Lee un archivo de verdad
		counter.setFileReader(realReader);
		
		int lines = counter.countLines("planetas.dat");
		
		assertTrue(lines < 0, "El fake no regresa lineas cuando lee un .dat");
		
	}
	
	@Test
	void countLinesOnMockTest() throws IOException {
		LinesCounter counter = new LinesCounter();
		
		Reader mockitoReader = Mockito.mock(Reader.class); //Creamos un mockito
		counter.setFileReader(mockitoReader);
		
		//Datos de prueba
		List<String> planetList = Arrays.asList(
				"Mercurio","Venus","Tierra",
				"Marte","Jupiter","Saturno",
				"Urano","Neptuno","Pluton");
		
		//Configuramos el mock...
		Mockito.when(mockitoReader.readLines("planetas.txt")).thenReturn(planetList);
		Mockito.when(mockitoReader.readLines("planetas.dat")).thenReturn(null);
		Mockito.when(mockitoReader.readLines("archivo_inexistente.txt")).thenThrow(new IOException());;
		
		int lines = 0;
		
		//probamos los casos
		lines = counter.countLines("planetas.txt");
		assertEquals(planetList.size(), lines, "archivo valido, regresa el numero de lineas");
		
		lines = counter.countLines("planetas.dat");
		assertEquals(-1, lines, "archivo no valido, regresa -1");
		
		lines = counter.countLines("archivo_inexistente.txt");
		assertEquals(-2, lines, "archivo no existente, regresa -2");
	}
	
	@Test
	void countLinesOnSpyTest() throws IOException {
		LinesCounter counter = new LinesCounter();
		
		//Creamos un reader
		Reader fileReader = new ResourceFileReader();
		
		//espiamos el reader
		Reader spyReader = Mockito.spy(fileReader);
		counter.setFileReader(spyReader);
		
		//Datos de prueba
		List<String> daysList = Arrays.asList(
				"Lunes", "Martes", "Miercoles", 
				"Jueves", "Viernes", "Sabado", "Domingo");
		
		int lines = 0;
		
		//probamos los casos
		lines = counter.countLines("planetas.txt");
		
		//Verificamos que se invoc� el metodo readLines con el parametro "planetas.txt"
		Mockito.verify(spyReader).readLines("planetas.txt");
		assertEquals(9, lines, "Hay 9 planetas");
		
		//Creamos un caso stub
		Mockito.doReturn(daysList).when(spyReader).readLines("dias.txt");
		lines = counter.countLines("dias.txt");
		
		Mockito.verify(spyReader).readLines("dias.txt");
		assertEquals(daysList.size(), lines, "archivo simulado");
		
	}
	
	

}
